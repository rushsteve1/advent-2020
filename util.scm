;;; A collection of various utility functions to be used across the days

(define-module (util)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:export (dbg
            inc
            dec
            println
            read-lines
            between
            between-eq
            unique
            product))

(define (inc x)
  (+ x 1))

(define (dec x)
  (- x 1))

;; Pretty print the argument and return it. Great when used inline.
(define (dbg arg)
  (if (procedure? arg)
      (lambda (. args) (dbg (apply arg args)))
      (begin (pretty-print arg) arg)))

(define (println . args)
  (for-each display args)
  (newline))

;; TODO debug this
(define (read-lines port)
  (letrec ((loop (lambda (l ls)
                   (if (eof-object? l)
                       ls
                       (loop (get-line port) (dbg (cons l ls)))))))
    (reverse (loop (get-line port) '()))))

;; Get the product of several lists
(define (product . args)
  (if (null? args)
      (list '())
      (apply append
             (map (lambda (rest)
                    (map (lambda (first)
                           (cons first rest))
                         (car args)))
                  (apply product (cdr args))))))

(define (between x low high)
  (and (< x high) (> x low)))

(define (between-eq x low high)
  (and (<= x high) (>= x low)))

(define (first-half lst)
  (take lst (/ (length lst) 2)))

(define (last-half lst)
  (take-right lst (/ (length lst) 2)))

(define (unique lst)
  (let loop ((lst (concatenate lst)) (res '()))
    (if (null? lst)
        (reverse res)
        (let ((c (car lst)))
          (loop (cdr lst) (if (member c res) res (cons c res)))))))
