#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/4
;;;
;;; Today seems a bit more focused on parsing out the data from the input, so
;;; I'm not gonna preprocess it again. Looking at the format it looks like I can
;;; parse it into an alist pretty easily.
;;;
;;; I didn't need to parse it into an Alist, but I had *SO* much trouble
;;; splitting this string properly, since apparently in Guile you can't split
;;; strings with strings, only with single characters.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (ice-9 regex)
 (util))

(define input
  (string-split
   (regexp-substitute/global
    #f
    "\n\n"
    (get-string-all
     (open-input-file "input.txt"))
    'pre "~" 'post)
   #\~))

(define (func ln ct)
  (if (and
       (string-contains ln "byr:")
       (string-contains ln "iyr:")
       (string-contains ln "eyr:")
       (string-contains ln "hgt:")
       (string-contains ln "hcl:")
       (string-contains ln "ecl:")
       (string-contains ln "pid:"))
      (+ ct 1)
      ct))

(fold func 0 input)
