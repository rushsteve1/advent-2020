#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/4#part2
;;;
;;; This one was also really tricky for me at first, but I took a break after
;;; spending a really long time on the first problem and was able to figure out
;;; a much better solution using match. Going back to something like my original
;;; Alist idea from part 1.
;;;
;;; I have to be careful of empty strings when using string-split though!


(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (ice-9 regex)
 (ice-9 match)
 (util))

(define input
  (string-split
   (regexp-substitute/global
    #f
    "\n\n"
    (get-string-all
     (open-input-file "input.txt"))
    'pre "~" 'post)
   #\~))

(define (parse-line ln)
  (map
   (lambda (s) (string-split s #\:))
   (filter
    (negate string-null?)
    (string-split ln char-set:whitespace))))

(define (year-between y low high)
  (let ((y (string->number y)))
    (and y (between-eq y low high))))

(define (valpair pair acc)
  (and acc
   (match pair
     (("byr" y) (year-between y 1920 2002))
     (("iyr" y) (year-between y 2010 2020))
     (("eyr" y) (year-between y 2020 2030))
     (("hgt" h) (let ((hd (string->number (string-drop-right h 2)))
                      (unit (string-take-right h 2)))
                  (and hd
                   (cond
                    ((string= unit "cm") (between-eq hd 150 193))
                    ((string= unit "in") (between-eq hd 59 76))
                    (else #f)))))
     (("hcl" h) (string-match "^#[0-9a-f]{6}$" h))
     (("ecl" e) (member e '("amb" "blu" "brn" "gry" "grn" "hzl" "oth")))
     (("pid" p) (and (= 9 (string-length p)) (string->number p)))
     (("cid" _) #t)
     (_ #f))))

(define (validate ln)
  (fold valpair #t (parse-line ln)))

(define (func ln ct)
  (if (and
       (string-contains ln "byr:")
       (string-contains ln "iyr:")
       (string-contains ln "eyr:")
       (string-contains ln "hgt:")
       (string-contains ln "hcl:")
       (string-contains ln "ecl:")
       (string-contains ln "pid:")
       (validate ln))
      (+ ct 1)
      ct))

(fold func 0 input)
