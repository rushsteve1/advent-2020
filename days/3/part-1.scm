#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/2
;;;
;;; Day 3, things seem to be getting a bit more complex. Or at least the input
;;; this time is more complex. I think the puzzle itself won't be too bad.
;;;
;;; Decided I wasn't gonna preprocess the input myself today. The input is
;;; simple so I think I don't need to.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (util))

;; Slope
(define right 3)
(define down  1)

(define input
 (string-split
  (get-string-all
   (open-input-file "input.txt"))
  #\newline))

(define len (string-length (car input)))

(let loop ((ls input) (x 0) (ct 0))
  (if (or (null? ls) (string-null? (car ls)))
      ct
      (loop
       (drop ls down)
       (+ x right)
       (if (equal? (string-ref (car ls) (modulo x len)) #\#)
           (+ ct 1)
           ct))))
