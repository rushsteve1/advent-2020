#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/2#part2
;;;
;;; This part 2 really builds off the previous, and thankfully my code was easy
;;; to modify. Just wrap up the previous loop in a function and fold over the
;;; possibilities.
;;;
;;; Higher-order functions are the best.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (util))

(define input
 (string-split
  (get-string-all
   (open-input-file "input.txt"))
  #\newline))

(define len (string-length (car input)))

(define (trees-hit right down)
 (let loop ((ls input) (x 0) (ct 0))
   (if (or (null? ls) (string-null? (car ls)))
       ct
       (loop
        (drop ls down)
        (+ x right)
        (if (equal? (string-ref (car ls) (modulo x len)) #\#)
            (+ ct 1)
            ct)))))

;; Right 1, down 1.
;; Right 3, down 1.
;; Right 5, down 1.
;; Right 7, down 1.
;; Right 1, down 2.

(fold
 (lambda (slope ct) (* ct (apply trees-hit slope)))
 1
 '((1 1) (3 1) (5 1) (7 1) (1 2)))
