#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/6
;;;
;;; I was crazy busy today and didn't get a chance to work on this until the
;;; evening. Thankfully today seems like a suprisingly straightforward problem
;;; and I did it without much difficulty.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (ice-9 regex)
 (util))

(define input
 (string-split
  (regexp-substitute/global
   #f
   "\n\n"
   (get-string-all
    (open-input-file "input.txt"))
   'pre "~" 'post)
  #\~))

(apply +
 (map
  (lambda (s)
    (length
     (unique
      (map
       (lambda (s) (string->list s))
       (string-split s char-set:whitespace)))))
  input))
