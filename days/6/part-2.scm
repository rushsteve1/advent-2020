#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/6#part2
;;;
;;; This one is a bit trickier but I think I can do it with set operations from
;;; SRFI-1, specifically the intersection.
;;;
;;; Once again I have to be careful of empty strings/lists when parsing!

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (ice-9 regex)
 (util))

(define input
 (string-split
  (regexp-substitute/global
   #f
   "\n\n"
   (get-string-all
    (open-input-file "input.txt"))
   'pre "~" 'post)
  #\~))

(define (intersect lsts)
  (apply lset-intersection (cons equal? (filter (negate null?) lsts))))

(define (func in)
 (map
  (lambda (s)
    (length
     (intersect
      (map
       (lambda (s) (filter (negate null?) (string->list s)))
       (string-split s char-set:whitespace)))))
  in))

(apply + (func input))
