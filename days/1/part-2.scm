#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/1#part2
;;;
;;; On to part 2! This is thankfully incredibly easy to do given the code I
;;; already have.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

(define input (load "input.scm"))

(apply * (find (lambda (l) (= (apply + l) 2020)) (product input input input)))
