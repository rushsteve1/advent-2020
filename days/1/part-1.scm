#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/1
;;;
;;; Day 1, so it begins. This is my first time doing AoC and I've decided to go
;;; with Scheme after being indecisive for a while. Hopefully I'll stick with it!
;;;
;;; This actually took me quite a while, but I figured a lot of things out.
;;; Wrestling with the Guile documentation hasn't been fun though...

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

;; This is MUCH simpler than loading and parsing from text
(define input (load "input.scm"))

(apply * (find (lambda (l) (= (apply + l) 2020)) (product input input)))

;; Alternative implementation based on an approach used by Gagbo
;;
;; cache = {}
;; for entry in list:
;;     if cache[2020 - entry] is not None:
;;         return (2020 - entry) * entry
;;     cache[entry] = True
;; raise InvalidValue("Ill formed list")

(use-modules (ice-9 vlist))

(let loop ((ls input) (table (alist->vhash '())))
  (let ((entry (car ls)))
   (if (vhash-assoc (- 2020 entry) table)
       (* entry (- 2020 entry))
       (loop (cdr ls)
             (vhash-cons entry #t table)))))
