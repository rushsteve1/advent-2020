#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/8#part2
;;;
;;; An interesting and tricky continuation of the previous problem, finding the
;;; broken instruction.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (util))

;; Input for part 1
(define file "input.txt")

(define input
  (map (lambda (ls) (cons (first ls) (string->number (last ls))))
   (map
    (lambda (s) (string-split s char-set:whitespace))
    (string-split
     (string-trim-both
      (get-string-all
       (open-input-file file)))
     #\newline))))

(define visited (list))

(define (mark-visited idx)
  (set! visited (cons idx visited)))

(define (evaluate input)
 (let loop ((index 0) (acc 0))
   (if (member index visited)
       #f
       (begin
        (mark-visited index)
        (if (>= index (length input))
            acc
            (let ((cur (list-ref input index)))
             (cond
              ((string= "acc" (car cur))
               (loop (+ index 1) (+ acc (cdr cur))))
              ((string= "jmp" (car cur))
               (loop (+ index (cdr cur)) acc))
              ((string= "nop" (car cur))
               (loop (+ index 1) acc)))))))))

(let loop ((inp (list-copy input)) (ind 0))
  (set! visited (list))
  (unless (>= ind (length inp))
     (let ((cur (list-ref inp ind)))
       (cond
        ((string= "jmp" (car cur))
         (list-set! inp ind (cons "nop" (cdr cur))))
        ((string= "nop" (car cur))
         (list-set! inp ind (cons "jmp" (cdr cur)))))
      (or (evaluate inp) (loop (list-copy input) (+ 1 ind))))))
