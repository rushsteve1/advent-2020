#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/8
;;;
;;; Catching back up, this one is interesting since it's a simple interpreter
;;; for an ASM-like language, which is always a fun little experiment.
;;; So this was fairly easy for me to write quickly.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (util))

;; Input for part 1
(define file "input.txt")

(define input
  (map (lambda (ls) (cons (first ls) (string->number (last ls))))
   (map
    (lambda (s) (string-split s char-set:whitespace))
    (string-split
     (string-trim-both
      (get-string-all
       (open-input-file file)))
     #\newline))))

(define visited (list))

(define (mark-visited idx)
  (set! visited (cons idx visited)))

(let loop ((index 0) (acc 0))
  (dbg (+ 1 index))
  (if (member index visited)
      acc
      (begin
       (mark-visited index)
       (if (>= index (length input))
           (dbg acc)
           (let ((cur (list-ref input index)))
            (cond
             ((string= "acc" (car cur))
              (loop (+ index 1) (+ acc (cdr cur))))
             ((string= "jmp" (car cur))
              (loop (+ index (cdr cur)) acc))
             ((string= "nop" (car cur))
              (loop (+ index 1) acc))))))))
