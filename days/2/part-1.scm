#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/2
;;;
;;; On to day 2!
;;;
;;; I've realized that one way to greatly simplfy some of the puzzles is to
;;; manually pre-process the input file into something more scheme-y and then
;;; simply load it.
;;; I did this using Vim macros, but I need to remember that Emacs EVIL's macro
;;; evaluation is VERY slow.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

(define input (load "input.scm"))

(define (func ct start end char str)
  (let ((count (string-count str char)))
    (if (and (>= count start) (<= count end))
        (+ ct 1)
        ct)))

(fold (lambda (l a) (apply func (cons a l))) 0 input)
