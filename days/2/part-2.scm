#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/2#part2
;;;
;;; This one was fairly straightforward given my existing strategy of using Fold.
;;;
;;; I came up with a good trick while doing this too, of using apply to expand
;;; out the arguments so I didn't have to use caddr and friends. I applied it to
;;; the last one too.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

(define input (load "input.scm"))

(define (func ct start end char str)
  (let ((a (string-ref str (- start 1)))
        (b (string-ref str (- end 1))))
    (cond
     ((equal? a b char) ct)
     ((or (equal? a char) (equal? b char)) (+ ct 1))
     (else ct))))

(fold (lambda (l a) (apply func (cons a l))) 0 input)
