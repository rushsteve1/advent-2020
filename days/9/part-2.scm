#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/9#part2
;;;
;;; This one naturaly expanded on the part 1, but really wasn't much difficulty
;;; either, only a handful more lines of code.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

(define input (load "input.scm"))

(define invalid
 (let loop ((lst input))
   (let* ((preamble (take lst 25))
          (sums (map (lambda (x) (apply + x)) (product preamble preamble)))
          (i (list-ref lst 25)))
     (if (memq i sums)
         (loop (cdr lst))
         i))))

(let loop ((lst input) (i 1))
  (let* ((taken (take lst i))
         (sorted (sort taken <))
         (sum (apply + taken)))
    (cond
     ((= sum invalid) (+ (first sorted) (last sorted)))
     ((> sum invalid) (loop (cdr lst) 1))
     ((< sum invalid) (loop lst (+ 1 i))))))
