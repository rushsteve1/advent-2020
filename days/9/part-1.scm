#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/9
;;;
;;; After doing the last 2 days at once yesterday, and both being quite tricky,
;;; today seems much easier which is a blessing.
;;;
;;; Actually this was shockingly easy, took me almost no time at all.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

(define input (load "input.scm"))

(let loop ((lst input))
  (let* ((preamble (take lst 25))
         (sums (map (lambda (x) (apply + x)) (product preamble preamble)))
         (i (list-ref lst 25)))
    (if (memq i sums)
        (loop (cdr lst))
        i)))
