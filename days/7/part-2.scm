#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/7#part2
;;;
;;; I accidentally wrote something very similar to this problem for part 1 after
;;; getting confused, so I generally know how to go about this.
;;;
;;; Math is hard though, took me forever to figure out a bug in there...

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 regex)
 (ice-9 vlist)
 (ice-9 textual-ports)
 (util))

(define input (load "input.scm"))

(define (trim str)
  (string-trim-both str (char-set #\space #\newline #\.)))

(define (num-bag str)
  (define ln (string-split (trim str) char-set:whitespace))
  (cons (string->number (first ln)) (string-join (cdr ln))))

(define (parse-line ln)
  (if (string-contains ln "other")
      '()
      (begin
       (let* ((debagged (regexp-substitute/global #f "bags?" ln 'pre 'post))
              (keyed (string-split
                      (regexp-substitute/global #f "contain" debagged 'pre "~" 'post) #\~)))
        (cons (trim (first keyed)) (map num-bag (string-split (last keyed) #\,)))))))

(define table (alist->vhash (filter (negate null?) (map parse-line input))))

(define (contains-tree color)
  (let ((bag (vhash-assoc color table)))
    (if bag
     (map (lambda (b) (cons (car b) (contains-tree (cdr b)))) (cdr bag))
     1)))

(define (calc-subtree tree)
  (cond
   ((list? tree) (+ (car tree) (* (car tree) (apply + (map calc-subtree (cdr tree))))))
   ((pair? tree) (* (car tree) (cdr tree)))))

(apply + (map calc-subtree (contains-tree "shiny gold")))
