#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/7
;;;
;;; I was so busy/lazy on the 7th that I wasn't able to get to this until the
;;; following day. Which is a shame since I was looking forward to a full week
;;; down. But oh well, better late than never.
;;;
;;; Today's puzzle is a tricky one through, and having the extra time to think
;;; about it has been helpful.
;;; I decided to do a bit of preprocessing, but not much since I think it's part
;;; of this problem.
;;;
;;; This one was reallyt tricky anyway, but I managed to get it. I don't think
;;; it's the most optimal solution, but it seems to work.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 regex)
 (ice-9 textual-ports)
 (util))

(define input (load "input.scm"))

(define (trim str)
  (string-trim-both str (char-set #\space #\newline #\.)))

(define (num-bag str)
  (define ln (string-split (trim str) char-set:whitespace))
  (cons (string->number (first ln)) (string-join (cdr ln))))

(define (parse-line ln)
  (if (string-contains ln "other")
      '()
      (begin
       (let* ((debagged (regexp-substitute/global #f "bags?" ln 'pre 'post))
              (keyed (string-split
                      (regexp-substitute/global #f "contain" debagged 'pre "~" 'post) #\~)))
        (cons (trim (first keyed)) (map num-bag (string-split (last keyed) #\,)))))))

(define table (filter (negate null?) (map parse-line input)))

(define (unique lst)
  (apply lset-adjoin (cons equal? (cons (list) lst))))

(define (bag-can-hold bag color)
  (find (lambda (b) (string= color (cdr b))) (cdr bag)))

(define (bags-containing color)
  (map car (filter (lambda (b) (bag-can-hold b color)) table)))

(length
 (unique
  (let loop ((parents (bags-containing "shiny gold")))
    (if (null? parents)
        (list)
        (append
         parents
         (concatenate
          (map (lambda (b) (loop (bags-containing b)))
               parents)))))))
