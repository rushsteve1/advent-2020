#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/11
;;;
;;; A neat one, I like cellular automata simulations like this. Kinda trickier,
;;; but I know how to approach them.
;;;
;;; After being tripped up by RPN for the upteenth time I finally wrote inc and
;;; dec functions for myself.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (util))

(define input
  (map string->list
    (string-split
     (string-trim-both
      (get-string-all
       (open-input-file "input.txt")))
     #\newline)))

(define floor #\.)
(define empty #\L)
(define occupied #\#)

(define (next-gen prev)
 (define max-x (dec (length (car prev))))
 (define max-y (dec (length prev)))

 (define (get-at x y)
   (if (or (< x 0) (> x max-x) (< y 0) (> y max-y))
       floor
       (list-ref (list-ref prev y) x)))

 (define (cells-around x y)
   (list
    (get-at (inc x) y)
    (get-at (dec x) y)
    (get-at x (inc y))
    (get-at x (dec y))
    (get-at (inc x) (inc y))
    (get-at (dec x) (dec y))
    (get-at (inc x) (dec y))
    (get-at (dec x) (inc y))))

 (define (count-around x y)
   (count (lambda (x) (equal? x occupied)) (cells-around x y)))

 (define (new-status x y)
   (define cell (get-at x y))
   (define count (count-around x y))
   (cond
    ((equal? floor cell) floor)
    ((equal? occupied cell) (if (>= count 4) empty occupied))
    ((equal? empty cell) (if (= count 0) occupied empty))))

 (map
  (lambda (y)
    (map
     (lambda (x) (new-status x y))
     (iota (length (list-ref prev y)))))
  (iota (length prev))))

(let loop ((gen input))
  (let ((next (next-gen gen)))
    (if (equal? gen next)
        (count (lambda (c) (equal? c occupied)) (concatenate next))
        (loop next))))
