#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/11#part2
;;;
;;; This was an interesting expansion on the previous part, conceptually simple
;;; but I had some trouble with it.
;;;
;;; While I was at it I switched around the lists to vectors in order to improve
;;; performance since it's doing a lot of indexed reads and length calculations

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (ice-9 textual-ports)
 (util))

(define text (get-string-all (open-input-file "input.txt")))

(define input
  (list->vector
   (map (lambda (s) (list->vector (string->list s)))
     (string-split
      (string-trim-both
       text)
      #\newline))))

(define floor #\.)
(define empty #\L)
(define occupied #\#)

(define (next-gen prev)
 (define max-x (dec (vector-length (vector-ref prev 1))))
 (define max-y (dec (vector-length prev)))


 (define (get-at x y)
   (if (or (< x 0) (> x max-x) (< y 0) (> y max-y))
       floor
       (vector-ref (vector-ref prev y) x)))

 (define (walker xx yy xmod ymod)
   (let loop ((x (+ xx xmod)) (y (+ yy ymod)))
     (cond
      ((or (< x 0) (> x max-x) (< y 0) (> y max-y)) #f)
      ((equal? floor (get-at x y)) (loop (+ x xmod) (+ y ymod)))
      (else (equal? (get-at x y) occupied)))))

 (define (cells-around x y)
   (list
    (walker x y 1 0)
    (walker x y -1 0)
    (walker x y 0 1)
    (walker x y 0 -1)
    (walker x y 1 1)
    (walker x y -1 1)
    (walker x y 1 -1)
    (walker x y -1 -1)))

 (define (count-around x y)
   (count (lambda (x) x) (cells-around x y)))

 (define (new-status x y)
   (define cell (get-at x y))
   (define count (count-around x y))
   (cond
    ((equal? floor cell) floor)
    ((equal? occupied cell) (if (>= count 5) empty occupied))
    ((equal? empty cell) (if (= count 0) occupied empty))))

 (list->vector
  (map
   (lambda (y)
     (list->vector
      (map
       (lambda (x) (new-status x y))
       (iota (vector-length (vector-ref prev y))))))
   (iota (vector-length prev)))))

(let loop ((gen input))
  (let ((next (next-gen gen)))
    (if (equal? gen next)
        (count
         (lambda (c) (equal? c occupied))
         (concatenate (map vector->list (vector->list next))))
        (loop next))))
