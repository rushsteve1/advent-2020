#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/5
;;;
;;; 5 Days in, 1/5 of the way there!
;;; Today is an interesting one since it deals with Binary Space Partitioning.
;;; Once again doing a bit of preprocessing since for this problem it's not
;;; really part of the problem
;;;
;;; I'm also going to minorly challenge myself to only use the geiser REPL.
;;; Previous days I used the REPL, but I'd also fire up a terminal and run the
;;; file to see bugs and stuff. Now I wanna do it all in the REPL.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

(define input (map string->list (load "input.scm")))

(define (row-col pass)
  (let loop ((pass pass) (rows (iota 128)) (cols (iota 8)))
    (if (null? pass)
        (list (car rows) (car cols))
        (case (car pass)
          ((#\F) (loop (cdr pass) (first-half rows) cols))
          ((#\B) (loop (cdr pass) (last-half rows) cols))
          ((#\L) (loop (cdr pass) rows (first-half cols)))
          ((#\R) (loop (cdr pass) rows (last-half cols)))))))

(define (id row col)
  (+ (* row 8) col))

(fold (lambda (x mx) (max mx (apply id (row-col x)))) 0 input)
