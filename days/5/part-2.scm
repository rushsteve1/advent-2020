#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/5#part2
;;;
;;; A fairly straightforward continuation of the last problem, gotta find your seat!
;;; This is one of those problems too where I think it's always going to be O(n)
;;; since you have to check for holes in the list.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

(define input (map string->list (load "input.scm")))

(define (row-col pass)
  (let loop ((pass pass) (rows (iota 128)) (cols (iota 8)))
    (if (null? pass)
        (list (car rows) (car cols))
        (case (car pass)
          ((#\F) (loop (cdr pass) (first-half rows) cols))
          ((#\B) (loop (cdr pass) (last-half rows) cols))
          ((#\L) (loop (cdr pass) rows (first-half cols)))
          ((#\R) (loop (cdr pass) rows (last-half cols)))))))

(define (id row col)
  (+ (* row 8) col))

(define ids (sort (map (lambda (x) (apply id (row-col x))) input) <))

(let loop ((a (first ids)) (b (second ids)) (ids (cdr ids)))
  (if (= 2 (- b a))
      (- b 1)
      (loop (first ids) (second ids) (cdr ids))))
