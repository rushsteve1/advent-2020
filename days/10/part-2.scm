#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/10#part2
;;;
;;; This problem was a LOT harder than the previous one. Both because it's
;;; inherently somewhat tricky, and because the obvious solution of using
;;; recursion is so slow that it's unusable. Seems like a lot of people took
;;; this approach and got burned by it, including me.
;;;
;;; This reddit comment ended up being a very valuable hint to how to do it properly.
;;; https://www.reddit.com/r/adventofcode/comments/kacdbl/2020_day_10c_part_2_no_clue_how_to_begin/gf9lzhd/?utm_source=reddit&utm_medium=web2x&context=3

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (srfi srfi-41)
 (ice-9 vlist)
 (util))

(define input (sort (load "input.scm") <))
(define highest (last input))

(let loop ((rem input)
           (prev 0)
           (table (alist->vhash (list (cons 0 1)))))
  (if (or (null? rem))
      (vhash-assoc highest table)
      (let* ((reachable
              (filter
               (lambda (x) (>= 3 (- x prev)))
               (stream->list (stream-take 3 (list->stream rem)))))
             (tab
              (fold
               (lambda (x acc)
                 (vhash-cons
                  x
                  (+
                   (cdr (or (vhash-assoc x acc) (cons 0 0)))
                   (cdr (vhash-assoc prev acc)))
                  (vhash-delete x acc)))
               table
               reachable)))
        ;; (dbg (vlist->list tab))
        (loop (cdr rem) (car rem) tab))))
