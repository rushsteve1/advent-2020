#!/usr/bin/env -S guile -s
!#

;;; https://adventofcode.com/2020/day/10
;;;
;;; This one feels way too straightfoward.
;;; I got tripped up a little because the starting and ending connections count
;;; too, but aside from that this problem was really simple.

(add-to-load-path "../..")
(use-modules
 (srfi srfi-1)
 (util))

(define input (sort (load "input.scm") <))

(apply
 *
 (let loop ((rem (cons 0 input)) (1j 0) (3j 1))
   (if (or (null? rem) (null? (cdr rem)))
       (list 1j 3j)
       (case (- (cadr rem) (car rem))
         ((1) (loop (cdr rem) (+ 1 1j) 3j))
         ((3) (loop (cdr rem) 1j (+ 1 3j)))))))
